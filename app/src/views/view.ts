
export abstract class View<T> {

    protected element: HTMLElement;

    constructor(seletor: string) {
        const elemento = document.querySelector(seletor);
        if (elemento) {
            this.element = elemento as HTMLElement;
        } else {
            throw Error(seletor + ' não existe no DOM.')
        }
    }

    // Os decorators são executados na ordem em que estão.
    // O código javascript criado, por outro lado, está na ordem inversa. 
    // @logarTempoDeExecucao(true)
    // @inspect()
    update(model: T): void {
        let template = this.template(model);
        this.element.innerHTML = template;
    }

    protected abstract template(model: T): string;
}