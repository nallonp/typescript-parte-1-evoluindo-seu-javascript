import { Imprimivel } from "./imprimivel";

export function imprimir(...objetos: Imprimivel[]) {
    objetos.forEach(o => console.log(o.paraTexto()));
}